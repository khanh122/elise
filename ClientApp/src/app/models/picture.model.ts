export interface CreatePicture {
    pictureName: string;
    pictureTypeId: number;
}