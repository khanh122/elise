import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private _httpClient: HttpClient) { }

  get(path: string, headers: HttpHeaders = null): Observable<any> {
    return this._httpClient.get(environment.apiUrl + path, { headers: headers })
  }

  post(path: string, body: any, headers: HttpHeaders = null): Observable<any> {
    return this._httpClient.post(environment.apiUrl + path, body, { headers: headers });
  }

  put(path: string, body: any, headers: HttpHeaders = null): Observable<any> {
    return this._httpClient.put(environment.apiUrl + path, body, { headers: headers })
  }

  public delete(path: string, headers: HttpHeaders = null): Observable<any> {
    return this._httpClient.delete(environment.apiUrl + path, { headers: headers });
  }
}
