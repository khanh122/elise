import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenericService {

  constructor(private _apiService: ApiService) { }

  getPicture() {
    return this._apiService.get('picture');
  }
}
