﻿using Elise.DataBinding;
using Elise.Domains;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Elise.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EliseController : ControllerBase
    {
        private readonly EliseUnitOfWork _eliseUnitOfWork;
        public EliseController(EliseUnitOfWork eliseUnitOfWork)
        {
            _eliseUnitOfWork = eliseUnitOfWork;
        }

        [Route("picture")]
        [HttpGet]
        public async Task<IActionResult> GetAllPicture()
        {
            return Ok(await _eliseUnitOfWork.PictureRepository.GetAllAsync());
        }

        [Route("picture")]
        [HttpPost]
        public async Task<IActionResult> CreatePicture(CreatePictureBindingModel createPicture)
        {
            var picture = new Picture
            {
                PictureName = createPicture.PictureName,
                PictureTypeId = createPicture.PictureTypeId
            };

            await _eliseUnitOfWork.PictureRepository.CreateAsync(picture);
            _eliseUnitOfWork.Save();

            return Ok();
        }
    }
}
