﻿namespace Elise.DataBinding
{
    public class CreatePictureBindingModel
    {
        public string PictureName { get; set; }
        public int PictureTypeId { get; set; }
    }
}
