﻿namespace Elise.Domains
{
    public class Blog : BaseEntity
    {
        public int BlogId { get; set; }
        public string BlogName { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
