﻿using System.Collections;
using System.Collections.Generic;

namespace Elise.Domains
{
    public class Category : BaseEntity
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public ICollection<Blog> Blogs { get; set; }
    }
}
