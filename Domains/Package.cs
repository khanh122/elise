﻿namespace Elise.Domains
{
    public class Package : BaseEntity
    {
        public int PackageId { get; set; }
        public double Price { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
