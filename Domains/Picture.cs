﻿namespace Elise.Domains
{
    public class Picture : BaseEntity
    {
        public int PictureId { get; set; }
        public string PictureName { get; set; }
        public int PictureTypeId { get; set; }
        public PictureType PictureType { get; set; }
    }
}
