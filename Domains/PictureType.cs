﻿
using System.Collections.Generic;

namespace Elise.Domains
{
    public class PictureType : BaseEntity
    {
        public int PictureTypeId { get; set; }
        public string PictureTypeName { get; set; }
        public ICollection<Picture> Pictures { get; set; }
    }
}
