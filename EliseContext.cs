﻿using Elise.Domains;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Elise
{
    public class EliseContext : DbContext
    {
        public EliseContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // base entity
            modelBuilder.Ignore<BaseEntity>();

            // picture entity
            modelBuilder.Entity<Picture>().HasKey(x => x.PictureId);
            modelBuilder.Entity<Picture>().HasOne(x => x.PictureType).WithMany(y => y.Pictures);
            modelBuilder.Entity<Picture>().Property(t => t.CreateDate).HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<Picture>().Property(t => t.UpdateDate).HasDefaultValueSql("GETDATE()");

            // picture type entity
            modelBuilder.Entity<PictureType>().HasKey(x => x.PictureTypeId);
            modelBuilder.Entity<PictureType>().HasMany(x => x.Pictures).WithOne(y => y.PictureType);
            modelBuilder.Entity<PictureType>().Property(t => t.CreateDate).HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<PictureType>().Property(t => t.UpdateDate).HasDefaultValueSql("GETDATE()");

            // category entity
            modelBuilder.Entity<Category>().HasKey(x => x.CategoryId);
            modelBuilder.Entity<Category>().Property(t => t.CreateDate).HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<Category>().Property(t => t.UpdateDate).HasDefaultValueSql("GETDATE()");

            // blog
            modelBuilder.Entity<Blog>().HasKey(x => x.BlogId);
            modelBuilder.Entity<Blog>().Property(t => t.CreateDate).HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<Blog>().Property(t => t.UpdateDate).HasDefaultValueSql("GETDATE()");

            // package
            modelBuilder.Entity<Package>().HasKey(x => x.PackageId);
            modelBuilder.Entity<Package>().Property(t => t.CreateDate).HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<Package>().Property(t => t.UpdateDate).HasDefaultValueSql("GETDATE()");
        }

        DbSet<Picture> Pictures { get; set; }
        DbSet<Blog> Blogs { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<PictureType> PictureTypes { get; set; }
    }
}
