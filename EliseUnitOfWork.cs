﻿using Elise.Domains;
using Elise.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Elise
{
    public class EliseUnitOfWork : IDisposable
    {
        private readonly EliseContext _eliseContext;

        private IGenericRepository<Blog> blogRepository;
        private IGenericRepository<Picture> pictureRepository;
        public EliseUnitOfWork(EliseContext eliseContext)
        {
            _eliseContext = eliseContext;
        }

        public IGenericRepository<Blog> BlogRepository
        {
            get
            {

                if (blogRepository == null)
                {
                    blogRepository = new GenericRepository<Blog>(_eliseContext);
                }

                return blogRepository;
            }
        }

        public IGenericRepository<Picture> PictureRepository
        {
            get
            {

                if (pictureRepository == null)
                {
                    pictureRepository = new GenericRepository<Picture>(_eliseContext);
                }

                return pictureRepository;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _eliseContext.Dispose();
                }
            }
            disposed = true;
        }

        public void Save()
        {
            _eliseContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void RejectChanges()
        {
            foreach (var entry in _eliseContext.ChangeTracker.Entries()
                  .Where(e => e.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }
    }
}
