﻿using Elise.Domains;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elise
{
    public static class MigrationManager
    {
        public static IWebHost MigrateDatabase(this IWebHost webHost)
        {
            using (var scope = webHost.Services.CreateScope())
            {
                using (var eliseContext = scope.ServiceProvider.GetRequiredService<EliseContext>())
                {
                    try
                    {
                        var categorySeddData = new List<Category>()
                        {
                             new Category { CategoryName = "BRANDS" },
                             new Category { CategoryName = "DESIGNS" },
                             new Category { CategoryName = "PHOTOGRAPHY" },
                             new Category { CategoryName = "PROJECT" },
                        };

                        var categories = eliseContext.Set<Category>();

                        var notDefined = categorySeddData.Where(x => !categories.Any(y => y.CategoryName == x.CategoryName));

                        if (notDefined.Any())
                        {
                            eliseContext.Set<Category>().AddRange(notDefined);
                            eliseContext.SaveChanges();
                        }

                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }

            return webHost;
        }
    }
}
