﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Elise.Repositories
{
    public class GenericRepository<Entity> : IGenericRepository<Entity> where Entity : class
    {
        public DbContext Context { get; }
        protected DbSet<Entity> dbSet;

        public GenericRepository(DbContext Context)
        {
            this.Context = Context;
            this.dbSet = Context.Set<Entity>();
        }

        public virtual Entity Create(Entity entity)
        {
            if (null == entity)
            {
                throw new ArgumentNullException(typeof(Entity).Name + " is null");
            }

            dbSet.Add(entity);

            return entity;
        }

        public virtual Task<Entity> CreateAsync(Entity entity)
        {
            if (null == entity)
            {
                throw new ArgumentNullException(typeof(Entity).Name + " is null");
            }

            return Task.Run(() =>
            {
                return this.Create(entity);
            });
        }

        public void Delete(Expression<Func<Entity, bool>> expression)
        {
            var entity = this.FirstOrDefault(expression);

            if (null != entity)
            {
                dbSet.Remove(entity);
            }
        }

        public Task DeleteAsync(Expression<Func<Entity, bool>> expression)
        {
            return Task.Run(() =>
            {
                var entity = this.FirstOrDefault(expression);

                if (null != entity)
                {
                    dbSet.Remove(entity);
                }
            });
        }

        public virtual Entity First(Expression<Func<Entity, bool>> expression)
        {
            var record = dbSet.Find(expression);
            return record;
        }
  
        public virtual Task<Entity> FirstAsync(Expression<Func<Entity, bool>> expression)
        {
            return Task.Run(() =>
            {
                IQueryable<Entity> query = dbSet;
                var record = query.FirstAsync(expression);
                return record;
            });
        }
  
        public virtual Entity FirstOrDefault(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes)
        {
            IQueryable<Entity> query = dbSet;

            if (null != includes)
            {
                foreach (var inclue in includes)
                {
                    query = query.Include(inclue);
                }
            }

            var record = query.FirstOrDefault(expression);
            return record;
        }

        public virtual Task<Entity> FirstOrDefaultAsync(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes)
        {
            return Task.Run(() =>
            {
                IQueryable<Entity> query = dbSet;

                if (null != includes)
                {
                    foreach (var inclue in includes)
                    {
                        query = query.Include(inclue);
                    }
                }

                var record = query.FirstOrDefaultAsync(expression);
                return record;
            });
        }

        public virtual IQueryable<Entity> GetAll(Expression<Func<Entity, bool>> predicate = null, bool isTracked = true)
        {
            IQueryable<Entity> query = dbSet;

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            if (!isTracked)
            {
                query.AsNoTracking();
            }

            return query;
        }

        public virtual Entity Single(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes)
        {
            IQueryable<Entity> query = dbSet;

            if (null != includes)
            {
                foreach (var inclue in includes)
                {
                    query = query.Include(inclue);
                }
            }

            var record = query.FirstOrDefault(expression);
            return record;
        }

        public virtual Task<Entity> SingleAsync(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes)
        {
            return Task.Run(() =>
            {
                IQueryable<Entity> query = dbSet;

                if (null != includes)
                {
                    foreach (var inclue in includes)
                    {
                        query = query.Include(inclue);
                    }
                }

                var record = query.SingleAsync(expression);
                return record;
            });
        }

        public virtual Entity SingleOrDefault(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes)
        {
            IQueryable<Entity> query = dbSet;

            if (null != includes)
            {
                foreach (var inclue in includes)
                {
                    query = query.Include(inclue);
                }
            }

            var record = query.SingleOrDefault(expression);
            return record;
        }
    
        public virtual Task<Entity> SingleOrDefaultAsync(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes)
        {
            return Task.Run(() =>
            {
                IQueryable<Entity> query = dbSet;

                if (null != includes)
                {
                    foreach (var inclue in includes)
                    {
                        query = query.Include(inclue);
                    }
                }

                var record = query.SingleOrDefaultAsync(expression);
                return record;
            });
        }

        public virtual Entity Update(Expression<Func<Entity, bool>> expression, Entity entityToUpdate)
        {
            var entityDetail = this.FirstOrDefault(expression);

            if (entityDetail == null)
            {
                throw new Exception("the object" + typeof(Entity).Name + " which need to update is not available while updating");
            }

            Context.Entry(entityDetail).State = EntityState.Detached;
            Context.Entry(entityToUpdate).State = EntityState.Modified;

            return entityToUpdate;
        }

        public virtual Task<Entity> UpdateAsync(Expression<Func<Entity, bool>> expression, Entity entityToUpdate)
        {
            return Task.Run(() =>
            {
                return this.Update(expression, entityToUpdate);
            }, default);
        }

        public virtual IEnumerable<Entity> CreateRange(IEnumerable<Entity> entities)
        {
            if (null == entities || !entities.Any())
            {
                throw new ArgumentNullException("entities are empty, can not add range");
            }

            dbSet.AddRange(entities);
            return entities;
        }

        public virtual Task<IEnumerable<Entity>> CreateRangeAsync(IEnumerable<Entity> entities)
        {
            return Task.Run(() =>
            {
                return this.CreateRange(entities);
            });
        }

        public virtual Task<IQueryable<Entity>> GetAllAsync(Expression<Func<Entity, bool>> expression = null, bool isTracked = true)
        {
            return Task.Run(() =>
            {
                return this.GetAll(expression, isTracked);

            });
        }

        public virtual void DeleteRange(Expression<Func<Entity, bool>> expression)
        {
            dbSet.RemoveRange(dbSet.Where(expression));
        }

        public virtual Task DeleteRangeAsync(Expression<Func<Entity, bool>> expression)
        {
            return Task.Run(() =>
            {
                this.DeleteRange(expression);
            });
        }

    }
}
