﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Elise.Repositories
{
    public interface IGenericRepository<Entity> where Entity : class
    {
        Entity Create(Entity entity);
        IEnumerable<Entity> CreateRange(IEnumerable<Entity> entities);
        Task<IEnumerable<Entity>> CreateRangeAsync(IEnumerable<Entity> entities);
        Task<Entity> CreateAsync(Entity entity);
        void Delete(Expression<Func<Entity, bool>> expression);
        Task DeleteAsync(Expression<Func<Entity, bool>> expression);
        void DeleteRange(Expression<Func<Entity, bool>> expression);
        Task DeleteRangeAsync(Expression<Func<Entity, bool>> expression);
        Entity Update(Expression<Func<Entity, bool>> expression, Entity entityToUpdate);
        Task<Entity> UpdateAsync(Expression<Func<Entity, bool>> expression, Entity entityToUpdate);
        Entity First(Expression<Func<Entity, bool>> expression);
        Task<Entity> FirstAsync(Expression<Func<Entity, bool>> expression);
        Entity FirstOrDefault(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes);
        Task<Entity> FirstOrDefaultAsync(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes);
        Entity Single(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes);
        Task<Entity> SingleAsync(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes);
        Entity SingleOrDefault(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes);
        Task<Entity> SingleOrDefaultAsync(Expression<Func<Entity, bool>> expression, params Expression<Func<Entity, object>>[] includes);
        IQueryable<Entity> GetAll(Expression<Func<Entity, bool>> predicate = null, bool isTracked = false);
        Task<IQueryable<Entity>> GetAllAsync(Expression<Func<Entity, bool>> predicate = null, bool isTracked = false);
    }
}
